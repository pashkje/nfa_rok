$(document).ready(function() {

    $('.js-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<img src="img/arrow-left.png" alt="" class="slider__arrow slider__arrow_left">',
        nextArrow: '<img src="img/arrow-right.png" alt="" class="slider__arrow slider__arrow_right">',
    });

});
